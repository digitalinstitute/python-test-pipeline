#
# Download Urban Observatory Data for the specified time period
#

from urllib.request import urlopen
from datetime import timedelta
import time
import csv
import bbox
from urllib.error import HTTPError
import os

# Get Data
baseAddr = 'http://uoweb1.ncl.ac.uk/api/v1/sensors/data/raw.csv?'
apiKey = '&api_key=7b1a9ytmmhdcruloy81icy1u2dfk04njuco308y9eaxt9vfygr79aiplatb5c71s9iq30bdlad7swin7qgi7c2nvdt'

bbox = bbox.newcastle_centre


def downloadUOData(startDate, numDays, monthName):

    csvData = []

    writeHeader = True
    
    for _ in range(0,numDays):
        startDay = str(startDate.day)
        startMonth = str(startDate.month)
        startYear = str(startDate.year)
                
        if (startDate.day < 10): 
            startDay = "0"+str(startDate.day)
            
        if (startDate.month < 10): 
            startMonth = "0"+str(startDate.month)
        
        startTime = '000000'
        endTime = '235959'
        startDateStr = startYear+startMonth+startDay+startTime
        endDateStr = startYear+startMonth+startDay+endTime
    
        query = '&start_time='+startDateStr+'&end_time='+endDateStr+'&sensor_type=Air_Quality-and-Weather'
        
        #Form the query string
        getString = baseAddr + bbox + query + apiKey
        #print ( getString )
        print("Date: " + startDate.strftime("%d/%m/%y"))

        # ------------------------------------------------------------------------ Send Download Request

        try:
            response = urlopen( getString )
            data = response.read().decode()
            cr = csv.reader(data.split('\n'), delimiter=',', quotechar='"')
            
            header=True
                
            for row in cr:
                if header:    #only add the first header to the csvdata
                    header = False
                    
                    if writeHeader:
                        row.append('Event')
                        csvData.append(row)
                        writeHeader = False
                    
                else:
                    if len(row) > 0:                
                        if row[9] == "NO2" or row[9] == "NO" or row[9] == "Rainfall" or row[9] == "Daily Accumulation Rainfall" or row[9] == "Wind Speed" or row[9] == "Temperature":
                            row.append(startDate.strftime("%d/%m/%y"))
                            csvData.append(row)
        except HTTPError:
            print("Server Error - Skipping this date")

        startDate = startDate + timedelta(days=1)
        
    #--------------------------------------------------------------------------------------- Output CSV File

    if not os.path.exists('data'):
        os.makedirs('data')

    if not os.path.exists('data\\Daily Data ' + startYear):
        os.makedirs('data\\Daily Data ' + startYear)

    filename = 'data\\Daily Data ' + startYear + '\\SensorData'+monthName+'.csv'
    ofile = open(filename, "w", newline='')
    writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
    
    for csvRow in csvData:
        writer.writerow(csvRow)
    ofile.close()
    
    del csvData[:]
    
    print(filename + " written at " + time.ctime())
