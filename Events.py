#
# Read NO/NO2/CO/Temperature/Humidity/ data from the UO JSON for the SC emotes
# Data for a specified set of events, exported to a CSV file called "AllEvents"
#

from urllib.request import urlopen
from datetime import date
import csv
import bbox

# Get Data
baseAddr = 'http://uoweb1.ncl.ac.uk/api/v1/sensors/data/raw.csv?'

bbox = bbox.gateshead_centre

class Event:
    def __init__(self, n,d):
        self.name = n
        self.date = d

events = []
#events.append(Event("Great North Run 16",   date(2016,9,11)))
#events.append(Event("Great North Run 17",   date(2017,9,10)))
#events.append(Event("Newcastle Pride 16",   date(2016,7,16)))
#events.append(Event("Newcastle Pride 17",   date(2017,7,22)))
#events.append(Event("Tyne & Wear Derby 16", date(2016,3,20)))
#events.append(Event("Coldest Day 16",       date(2016,2,17)))
#events.append(Event("Hottest Day 16",       date(2016,8,2)))
events.append(Event("Coldest Day 17",       date(2017,2,17)))
events.append(Event("Hottest Day 17",       date(2017,8,2)))

startTime = '070000'
endTime = '190000'

apiKey = '&api_key=7b1a9ytmmhdcruloy81icy1u2dfk04njuco308y9eaxt9vfygr79aiplatb5c71s9iq30bdlad7swin7qgi7c2nvdt'

csvData = [];

#-------------------------------------------------------------------------------------------- The Event

writeHeader = True

for event in events:
    startDay = str(event.date.day)
    startMonth = str(event.date.month)
    startYear = str(event.date.year)
    
    if event.date.day < 10: 
        startDay = "0"+str(event.date.day)
        
    if event.date.month < 10: 
        startMonth = "0"+str(event.date.month)
        
    startDate = startYear+startMonth+startDay+startTime
    endDate = startYear+startMonth+startDay+endTime
    
    query = '&start_time='+startDate+'&end_time='+endDate+'&sensor_type=Air_Quality'
    
    #Form the query string
    getString = baseAddr + bbox + query + apiKey
    #print ( getString )
    
    print ("Event: " + event.name + ", Date: " + event.date.strftime("%d/%m/%y"))
    
    #Send GET query to the server
    response = urlopen( getString )
    data = response.read().decode()
    
    cr = csv.reader(data.split('\n'), delimiter=',', quotechar='"')
    
    header=True
        
    for row in cr:
        if header==True:    #only add the first header to the csvdata
            header=False
            
            if writeHeader==True:
                row.append('Event')
                csvData.append(row)
                writeHeader=False
                print(">>> writing header")
            
        else:
            if len(row) > 0:
                row.append(event.name)
                csvData.append(row)

#-------------------------------------------------------------------------------------------------------------

ofile = open('data\\AllEvents.csv', "w", newline='')
writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

for csvRow in csvData:
    #print(csvRow)
    writer.writerow(csvRow)

ofile.close()

print ( "Done" )