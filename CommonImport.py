monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

class AverageReading:
    def __init__(self):
        self.averages = 0
        self.count = 0

    def AddValue(self, value):
        self.averages += value
        self.count += 1

    @property
    def GetAverage(self):
        if self.count == 0:
            return 0
        else:
            return self.averages / self.count
