# README #

Test Pipeline for downloading and processing a year's worth of Urban Observatory data

### How it works ###

Executing Main.py will download all Urban Observatory air quality data for the specified year. 
Each day is downloaded separately, and then the selected data is stored in one CSV file for each month.

Currently, only NO, NO2, Rainfall, Temperature and Wind Speed data is stored.
The option to specify data types may be added at a later date.


Scripts will then calculate daily and hourly averages,so that reports can be produced on a smaller dataset. 
Linear regression is also calculated, including p- and r-squared values for the data.

### Author ###

* Mike Simpson (mike.simpson@ncl.ac.uk)