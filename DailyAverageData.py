#
# Calculate Daily Average Data from Hourly Average Data
#

import time
import csv
from CommonImport import AverageReading

class DailyData:
    def __init__(self, d):
        self.date = d     
        self.hourlyAverages = []
        self.averageTemp = AverageReading()
        self.averageNO2 = AverageReading()
        self.averageWindspeed = AverageReading()
        self.averageRainfall = AverageReading()
        self.averageAccRainfall = AverageReading()
        self.averageNO = AverageReading()
        
    def AddReading(self, n2, n, t, w, r, ar):
        self.averageNO2.AddValue(n2)
        self.averageNO.AddValue(n)
        self.averageTemp.AddValue(t)
        self.averageWindspeed.AddValue(w)
        self.averageRainfall.AddValue(r)
        self.averageAccRainfall.AddValue(ar)


def ProcessDailyAverageData(year):

    header = True
    outputData = []
    
    # --------------------------------------------------------------------------------- Load CSV
    
    with open('data\Daily Data ' + str(year) + '\Averages\HourlyAverageData.csv', 'r') as ifile:
        csvReader = csv.reader(ifile, delimiter=',', quotechar='"')
            
        for row in csvReader:    
            # print(row)
            if header:
                header = False
            else:
                reading_date = row[0]
                                     
                dataFound = False
                    
                for dataX in outputData:
                    if (dataX.date == reading_date):
                        dataObject = dataX
                        dataFound = True
                
                if (dataFound == False):
                    dataObject = DailyData(reading_date)
                    outputData.append(dataObject)
                
                # ---------------------------------------------------------------------
                
                if dataObject == 0:
                    print ("FAIL")
                else:
                    # temp, no2, no, windspeed, rainfall, acc. rainfall
                    dataObject.AddReading(float(row[2]), float(row[3]), float(row[4]), float(row[5]), float(row[6]), float(row[7]))
                
    # ------------------------------------------------------------------------------------------------------------------
                
    ofile = open('data\Daily Data ' + str(year) + '\Averages\DailyAverageData.csv', "w", newline='')
    writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
    
    writer.writerow(["Date", "NO2 (ugm-3)", "NO (ugm-3)", "Temp (Celcius)", "Windspeed (mph)", "Rainfall (mm)", "Daily Acc. Rainfall (mm)"])
    
    for dataObj in outputData:
        
        csv_date = dataObj.date
               
        csvRow = []
        csvRow.append(csv_date)
        csvRow.append(dataObj.averageNO2.GetAverage)
        csvRow.append(dataObj.averageNO.GetAverage)
        csvRow.append(dataObj.averageTemp.GetAverage)
        csvRow.append(dataObj.averageWindspeed.GetAverage)
        csvRow.append(dataObj.averageRainfall.GetAverage)
        csvRow.append(dataObj.averageAccRainfall.GetAverage)
        
        # print(csvRow)
        writer.writerow(csvRow)
    
    ofile.close()
    
    print("Daily Average Data processed at " + time.ctime())
