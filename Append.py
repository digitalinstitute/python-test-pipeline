#
# Append all of the CSV files in a folder in a single output CSV
#

import csv
import pathlib


def AppendCSVFiles(source, destination):

    # ----------------------------------------------------------------------------------------- Load CSV

    outputData = []

    first = True
    header = True

    target_dir = source

    currentDirectory = pathlib.Path(target_dir)

    for currentFile in currentDirectory.iterdir():
        print(currentFile)

        with open(currentFile, 'r') as ifile:
            csvReader = csv.reader(ifile, delimiter=',', quotechar='"')

            header = True

            for row in csvReader:
                if (header):
                    header = False
                    if (first):
                        first = False
                        outputData.append(row)
                else:
                    outputData.append(row)

    # ----------------------------------------------------------------------------------------- Write CSV

    ofile = open(destination, "w", newline='')
    writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

    for dataObj in outputData:
            writer.writerow(dataObj)
    ofile.close()

    print("Done")
