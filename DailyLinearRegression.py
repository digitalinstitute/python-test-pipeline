#
# Calculate R-squared value for different sensor types.
#

import time
import csv
from datetime import datetime
from scipy import stats
from CommonImport import monthNames


def ProcessDailyLinearRegression(year):
    no2_index = 0
    no_index = 0
    temp_index = 0
    wind_index = 0
    rainfall_index = 0
    acc_rainfall_index = 0

    rSquaredData = []
    pValueData = []

    firstPass = True

    for month in range(0, 12):
        with open('data\Daily Data ' + str(year) + '\Averages\DailyAverageData.csv', 'r') as ifile:
            csvReader = csv.reader(ifile, delimiter=',', quotechar='"')
            header = True

            monthly_no2_levels = []
            monthly_no_levels = []
            monthly_temperature_levels = []
            monthly_wind_levels = []
            monthly_rainfall_levels = []
            monthly_acc_rainfall_levels = []

            for row in csvReader:
                if header:
                    if firstPass:
                        index = 0
                        for elem in row:
                            if elem == "NO2 (ugm-3)":
                                no2_index = index
                            if elem == "NO (ugm-3)":
                                no_index = index
                            elif elem == "Temp (Celcius)":
                                temp_index = index
                            elif elem == "Windspeed (mph)":
                                wind_index = index
                            elif elem == "Rainfall (mm)":
                                rainfall_index = index
                            elif elem == "Daily Acc. Rainfall (mm)":
                                acc_rainfall_index = index

                            index = index + 1

                        firstPass = False

                    header = False
                else:
                    readingDate = datetime.strptime(row[0], '%Y-%m-%d')
                    readingMonth = readingDate.month

                    if (readingMonth - 1) == month:
                        monthly_no2_levels.append(float(row[no2_index]))
                        monthly_no_levels.append(float(row[no_index]))
                        monthly_temperature_levels.append(float(row[temp_index]))
                        monthly_wind_levels.append(float(row[wind_index]))
                        monthly_rainfall_levels.append(float(row[rainfall_index]))
                        monthly_acc_rainfall_levels.append(float(row[acc_rainfall_index]))

            gradient, intercept, r_value, p_value, std_err = stats.linregress(monthly_no2_levels,
                                                                              monthly_temperature_levels)
            rTemp = r_value ** 2
            pTemp = p_value

            gradient, intercept, r_value, p_value, std_err = stats.linregress(monthly_no2_levels,
                                                                              monthly_wind_levels)
            rWind = r_value ** 2
            pWind = p_value

            gradient, intercept, r_value, p_value, std_err = stats.linregress(monthly_no2_levels,
                                                                              monthly_rainfall_levels)
            rRain = r_value ** 2
            pRain = p_value

            gradient, intercept, r_value, p_value, std_err = stats.linregress(monthly_no2_levels,
                                                                              monthly_acc_rainfall_levels)
            rAccRain = r_value ** 2
            pAccRain = p_value

            rSquaredData.append([monthNames[month], str(rTemp), str(rWind), str(rRain), str(rAccRain)])
            pValueData.append([monthNames[month], str(pTemp), str(pWind), str(pRain), str(pAccRain)])

            del monthly_no2_levels[:]
            del monthly_no_levels[:]
            del monthly_temperature_levels[:]
            del monthly_wind_levels[:]
            del monthly_rainfall_levels[:]
            del monthly_acc_rainfall_levels[:]

    # --------------------------------------------------------------------------- rSquared Values

    ofile = open('data\Daily Data ' + str(year) + '\Averages\DailyRSquared.csv', "w", newline='')
    writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

    writer.writerow(
        ["Month", "R-squared (Temperature)", "R-squared (Wind)", "R-squared (Rainfall)", "R-squared (Acc. Rainfall)"])

    for row in rSquaredData:
        writer.writerow(row)
    ofile.close()

    # ---------------------------------------------------------------------------------- p Values

    ofile = open('data\Daily Data ' + str(year) + '\Averages\DailyPValues.csv', "w", newline='')
    writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

    writer.writerow(
        ["Month", "P Value (Temperature)", "P Value (Wind)", "P Value (Rainfall)", "P Value (Acc. Rainfall)"])

    for row in pValueData:
        writer.writerow(row)
    ofile.close()

    # ------------------------------------------------------------------------------------------

    print("Daily Linear Regression Data processed at " + time.ctime())
