#
# Master Pipeline for Downloading and Processing UO Sensor Data
#

import time
 
from DownloadController import DownloadSensorData
from DailyLinearRegression import ProcessDailyLinearRegression
from HourlySensorData import ProcessHourlySensorData
from HourlyAverageData import ProcessHourlyAverageData
from DailySensorData import ProcessDailySensorData
from DailyAverageData import ProcessDailyAverageData
from HourlyLinearRegression import ProcessHourlyLinearRegression
from Append import AppendCSVFiles

year = 2016

print("Started at " + time.ctime())
 
DownloadSensorData(year)
ProcessHourlySensorData(year)
AppendCSVFiles('data\Daily Data ' + str(year) + '\Averages\Months',
               'data\Daily Data ' + str(year) + '\Averages\HourlySensorData.csv')
ProcessHourlyAverageData(year)
ProcessDailySensorData(year)
ProcessDailyAverageData(year)
ProcessDailyLinearRegression(year)
ProcessHourlyLinearRegression(year)
 
print("Finished at " + time.ctime())
