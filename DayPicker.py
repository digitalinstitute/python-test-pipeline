#
# Extract data for a specific date from the downloaded sensor data
#

import os
import csv
from CommonImport import monthNames

day = 22
month = 7
year = 2017

doHeader = True
outputData = []
monthName = monthNames[month - 1]

# ------------------------------------------------------------------- Load CSV

filename = 'data\Daily Data ' + str(year) + '\SensorData' + monthName + '.csv'
print("Loading: " + filename)

with open(filename, 'r') as ifile:
    csvReader = csv.reader(ifile, delimiter=',', quotechar='"')

    dateIndex = 0

    for row in csvReader:

        if doHeader:
            header = row
            doHeader = False

            idx = 0
            for column in row:
                if column == 'Timestamp':
                    dateIndex = idx
                idx = idx + 1

        else:
            timestamp = row[dateIndex].split(" ")
            reading_date = timestamp[0]

            dateObj = reading_date.split("-")

            if int(dateObj[0]) == year and int(dateObj[1]) == month and int(dateObj[2]) == day:
                outputData.append(row)

# ----------------------------------------------------------------------------------------------------------------------

if not os.path.exists('data\Dates'):
    os.makedirs('data\Dates')

dayStr = str(day)
monthStr = str(month)

if day < 10:
    dayStr = "0" + dayStr

if month < 10:
    monthStr = "0" + monthStr

filename = 'data\Dates\DailyData' + dayStr + monthStr + str(year) + '.csv'
print("Writing: " + filename)

ofile = open(filename, "w", newline='')

writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

writer.writerow(header)

for dataRow in outputData:
    writer.writerow(dataRow)
ofile.close()

print("Done")
