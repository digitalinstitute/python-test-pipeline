#
# Calculate the Hourly Averages across all sensors from the Downloaded Data
#

import time
import csv
from CommonImport import monthNames, AverageReading


class HourlyAverage:
    def __init__(self, h):
        self.hour = h
        self.averageTemp = AverageReading()
        self.averageNO2 = AverageReading()
        self.averageWindspeed = AverageReading()
        self.averageRainfall = AverageReading()
        self.averageAccRainfall = AverageReading()
        self.averageNO = AverageReading()
        
    def AddReading(self, name, value):
        if (name=="Temperature"):
            self.averageTemp.AddValue(value)
        elif (name=="NO2"):
            self.averageNO2.AddValue(value)
        elif (name=="Wind Speed"):
            self.averageWindspeed.AddValue(value)
        elif (name=="Rainfall"):
            self.averageRainfall.AddValue(value)
        elif (name=="Daily Accumulation Rainfall"):
            self.averageAccRainfall.AddValue(value)
        elif (name=="NO"):
              self.averageNO.AddValue(value)
        else:
            print ("NOPE! :: " + name)


class DailyData:
    def __init__(self, d):
        self.date = d      
        self.hourlyAverages = []
        
        for i in range(0,24):
            self.hourlyAverages.append(HourlyAverage(i))
            
    def AddReading(self, time, name, value):
        self.hourlyAverages[time].AddReading(name, value)


def ProcessHourlyAverageData(year):

    outputData = []
    
    for month in monthNames:
        filename = 'data\Daily Data ' + str(year) + '\SensorData'+month+'.csv'
        print(time.ctime() + " - Processing: " + filename)
        
        header = True

        # ------------------------------------------------------------------------ Load CSV

        with open(filename, 'r') as ifile:
            csvReader = csv.reader(ifile, delimiter=',', quotechar='"')
            
            for row in csvReader:    
                # print(row)
                if header:
                    idx = 0
                    for column in row:
                        if column == "name":
                            nameIndex = idx
                        idx = idx+1
                    header = False
                else:
                    # print ("timestamp: " + row[11])
                    timestamp = row[11].split(" ")
                    
                    reading_date = timestamp[0]
                    reading_time = timestamp[1]
                    
                    reading_hour = int(reading_time.split(":")[0])
                    
                    sensorName = row[nameIndex]
                    
                    if sensorName != "aq_mesh1758150":      # exclude faulty sensor!
                        dataObject = 0
                        
                        # -------------------------------------------- Check if Sensor data for that date, if not create
                        
                        dataFound = False
                        
                        for dataX in outputData:
                            if dataX.date == reading_date:
                                dataObject = dataX
                                dataFound = True
                                break
                        
                        if dataFound == False:
                            dataObject = DailyData(reading_date)
                            outputData.append(dataObject)
                        
                        # ---------------------------------------------------------------------
                        
                        if dataObject == 0:
                            print ("FAIL")
                        else:
                            dataObject.AddReading(reading_hour, row[9], float(row[12])) # time, name, value
                        
                        # print ("Do Stuff")
            
    # ------------------------------------------------------------------------------------- Write CSV
    
    ofile = open('data\Daily Data ' + str(year) + '\Averages\HourlyAverageData.csv', "w", newline='')
    writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
    
    writer.writerow(["Date", "Hour", "NO2 (ugm-3)", "NO (ugm-3)", "Temp (Celcius)",
                     "Windspeed (mph)", "Rainfall (mm)", "Daily Acc. Rainfall (mm)"])

    for dataObj in outputData:
        
        csv_date = dataObj.date
        
        for i in range(0, 24):
            csv_hour = dataObj.hourlyAverages[i].hour 
            
            csvRow = []
            
            csvRow.append(csv_date)
            csvRow.append(csv_hour)
            
            csvRow.append(dataObj.hourlyAverages[i].averageNO2.GetAverage)            # NO2
            csvRow.append(dataObj.hourlyAverages[i].averageNO.GetAverage)             # NO
            csvRow.append(dataObj.hourlyAverages[i].averageTemp.GetAverage)           # Temp
            csvRow.append(dataObj.hourlyAverages[i].averageWindspeed.GetAverage)      # Windspeed
            csvRow.append(dataObj.hourlyAverages[i].averageRainfall.GetAverage)       # Rainfall
            csvRow.append(dataObj.hourlyAverages[i].averageAccRainfall.GetAverage)    # Daily Acc. Rainfall
            
            # print(csvRow)
            writer.writerow(csvRow)
    
    ofile.close()
    
    print("Hourly Average Data processed at " + time.ctime())
