#
# Calculate Daily Sensor Data from Hourly Sensor Data
#

import time
import csv
from CommonImport import AverageReading


class DailyData:
    def __init__(self, n, d, lat, lon, v):
        self.date = d     
        self.name = n
        self.latitude = lat 
        self.longitude = lon
        self.variable = v
        
        self.hourlyAverages = []
        self.averageReading = AverageReading()
        
    def AddReading(self, r):
        self.averageReading.AddValue(r)


def ProcessDailySensorData(year):

    header = True
    outputData = []
    
    print("Loading Hourly Sensor Data")
    
    # -------------------------------------------------------------------------------- Load CSV
    
    with open('data\Daily Data ' + str(year) + '\Averages\HourlySensorData.csv', 'r') as ifile:
        csvReader = csv.reader(ifile, delimiter=',', quotechar='"')
            
        for row in csvReader:    
            #print(row)
            if (header):
                header = False
            else:
                sensorName = row[0]
                latitude = row[1]
                longitude = row[2]
                reading_date = row[3]
                variable = row[5]
                                     
                dataFound = False
                    
                for dataX in outputData:
                    if (dataX.date == reading_date and dataX.name == sensorName):
                        dataObject = dataX
                        dataFound = True
                        break
                
                if (dataFound == False):
                    dataObject = DailyData(sensorName, reading_date, latitude, longitude, variable)
                    outputData.append(dataObject)
                
                # ---------------------------------------------------------------------------------
                
                if dataObject == 0:
                    print("FAIL")
                else:
                    dataObject.AddReading(float(row[6]))
                
    # -----------------------------------------------------------------------------------------------------------------
    
    print("Writing Daily Sensor Data")
                
    ofile = open('data\Daily Data ' + str(year) + '\Averages\DailySensorData.csv', "w", newline='')
    writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)
    
    writer.writerow(["Name", "latitude", "longitude", "Date", "Variable", "Value"])
    
    for dataObj in outputData:
        
        csv_date = dataObj.date
               
        csvRow = []
        csvRow.append(csv_date)
        csvRow.append(dataObj.name)
        csvRow.append(dataObj.latitude)
        csvRow.append(dataObj.longitude)
        csvRow.append(dataObj.variable)
        csvRow.append(dataObj.averageReading.GetAverage) #NO2
        
        #print(csvRow)
        writer.writerow(csvRow)
    
    ofile.close()
    
    print("Daily Sensor Data processed at " + time.ctime())
