#
# Calculate R-squared value for different sensor types.
#

import time
import csv
from datetime import datetime
from scipy import stats
from CommonImport import monthNames


def ProcessHourlyLinearRegression(year):
    no2_index = 0
    temp_index = 0
    wind_index = 0
    rainfall_index = 0
    acc_rainfall_index = 0

    rSquaredData = []
    pValueData = []

    firstPass = True

    for month in range(0, 12):
        for hour in range(0, 24):
            with open('data\Daily Data ' + str(year) + '\Averages\HourlyAverageData.csv', 'r') as ifile:
                csvReader = csv.reader(ifile, delimiter=',', quotechar='"')
                header = True

                hourly_no2_levels = []
                hourly_temperature_levels = []
                hourly_wind_levels = []
                hourly_rainfall_levels = []
                hourly_acc_rainfall_levels = []

                for row in csvReader:
                    if header:
                        if firstPass:
                            index = 0
                            for elem in row:
                                if elem == "NO2 (ugm-3)":
                                    no2_index = index
                                elif elem == "Temp (Celcius)":
                                    temp_index = index
                                elif elem == "Windspeed (mph)":
                                    wind_index = index
                                elif elem == "Rainfall (mm)":
                                    rainfall_index = index
                                elif elem == "Daily Acc. Rainfall (mm)":
                                    acc_rainfall_index = index

                                index = index + 1

                            firstPass = False

                        header = False
                    else:
                        readingDate = datetime.strptime(row[0], '%Y-%m-%d')
                        readingMonth = readingDate.month

                        readingHour = int(row[1])

                        if (readingMonth - 1) == month and readingHour == hour:
                            hourly_no2_levels.append(float(row[no2_index]))
                            hourly_temperature_levels.append(float(row[temp_index]))
                            hourly_wind_levels.append(float(row[wind_index]))
                            hourly_rainfall_levels.append(float(row[rainfall_index]))
                            hourly_acc_rainfall_levels.append(float(row[acc_rainfall_index]))

                gradient, intercept, r_value, p_value, std_err = stats.linregress(hourly_no2_levels,
                                                                                  hourly_temperature_levels)
                rTemp = r_value ** 2
                pTemp = p_value

                gradient, intercept, r_value, p_value, std_err = stats.linregress(hourly_no2_levels,
                                                                                  hourly_wind_levels)
                rWind = r_value ** 2
                pWind = p_value

                gradient, intercept, r_value, p_value, std_err = stats.linregress(hourly_no2_levels,
                                                                                  hourly_rainfall_levels)
                rRain = r_value ** 2
                pRain = p_value

                gradient, intercept, r_value, p_value, std_err = stats.linregress(hourly_no2_levels,
                                                                                  hourly_acc_rainfall_levels)
                rAccRain = r_value ** 2
                pAccRain = p_value

                rSquaredData.append([hour, monthNames[month], str(rTemp), str(rWind), str(rRain), str(rAccRain)])
                pValueData.append([hour, monthNames[month], str(pTemp), str(pWind), str(pRain), str(pAccRain)])

                del hourly_no2_levels[:]
                del hourly_temperature_levels[:]
                del hourly_wind_levels[:]
                del hourly_rainfall_levels[:]
                del hourly_acc_rainfall_levels[:]

    # --------------------------------------------------------------------------- rSquared Values

    ofile = open('data\Daily Data ' + str(year) + '\Averages\HourlyRSquared.csv', "w", newline='')
    writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

    writer.writerow(["Hour", "Month", "R-squared (Temperature)", "R-squared (Wind)", "R-squared (Rainfall)",
                     "R-squared (Acc. Rainfall)"])

    for row in rSquaredData:
        writer.writerow(row)
    ofile.close()

    # ---------------------------------------------------------------------------------- p Values

    ofile = open('data\Daily Data ' + str(year) + '\Averages\HourlyPValues.csv', "w", newline='')
    writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

    writer.writerow(
        ["Hour", "Month", "P Value (Temperature)", "P Value (Wind)", "P Value (Rainfall)", "P Value (Acc. Rainfall)"])

    for row in pValueData:
        writer.writerow(row)
    ofile.close()

    # ------------------------------------------------------------------------------------------

    print("Hourly Linear Regression Data processed at " + time.ctime())
