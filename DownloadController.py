#
# Download All Urban Observatory Data for the specified Year
#

import Download
from CommonImport import monthNames
from datetime import date
import time

months = [[1, 31], [2, 28], [3, 31], [4, 30], [5, 31], [6, 30], [7, 31], [8, 31], [9, 30], [10, 31], [11, 30], [12, 31]]


def DownloadSensorData(year):
    for month in months:
        startDate = date(year, month[0], 1)
        numDays = month[1]

        Download.downloadUOData(startDate, numDays, monthNames[month[0] - 1])

    print("Sensor Data Downloaded at " + time.ctime())
