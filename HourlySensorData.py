#
# Calculate the Hourly Averages for each sensor from the Downloaded Data
#

import time
import csv
import os
from CommonImport import monthNames, AverageReading


class HourlyAverage:
    def __init__(self, h):
        self.hour = h
        self.averageReading = AverageReading()

    def AddReading(self, value):
        self.averageReading.AddValue(value)


class DailyData:
    def __init__(self, n, d, lat, lon, p):
        self.name = n
        self.date = d
        self.latitude = lat
        self.longitude = lon
        self.parameter = p
        self.hourlyAverages = []

        for i in range(0, 24):
            self.hourlyAverages.append(HourlyAverage(i))

    def AddReading(self, time, value):
        self.hourlyAverages[time].AddReading(value)


def ProcessHourlySensorData(year):
    for month in monthNames:
        outputData = []

        # --------------------------------------------------------------------------------- Load CSV
        filename = 'data\\Daily Data ' + str(year) + '\\SensorData' + month + '.csv'
        print("Processing: " + filename + " - " + time.ctime())

        header = True

        with open(filename, 'r') as ifile:
            csvReader = csv.reader(ifile, delimiter=',', quotechar='"')

            for row in csvReader:
                # print(row)
                if (header):
                    # print ("Do Header")
                    idx = 0
                    for column in row:
                        if (column == "name"):
                            nameIndex = idx
                        idx = idx + 1
                    header = False
                else:
                    # print ("timestamp: " + row[11])
                    timestamp = row[11].split(" ")

                    if len(timestamp) > 1:
                        reading_date = timestamp[0]
                        reading_time = timestamp[1]

                        reading_hour = int(reading_time.split(":")[0])

                        sensorName = row[nameIndex]
                        parameter = row[9]

                        if (sensorName != "aq_mesh1758150"):
                            dataObject = 0

                            # ---------------------------------------- Check if Sensor data for that date, if not create

                            dataFound = False

                            for dataX in outputData:
                                if (dataX.date == reading_date and dataX.name == sensorName
                                        and dataX.parameter == parameter):

                                    dataObject = dataX
                                    dataFound = True
                                    break

                            if dataFound == False:
                                latitude = row[8]
                                longitude = row[7]

                                dataObject = DailyData(sensorName, reading_date, latitude, longitude, parameter)
                                outputData.append(dataObject)

                            # ---------------------------------------------------------------------

                            if dataObject == 0:
                                print("FAIL")
                            else:
                                dataObject.AddReading(reading_hour, float(row[12]))  # time, value

                            # print ("Do Stuff")

        # ----------------------------------------------------------------------------------------- Write CSV

        if not os.path.exists('data\\Daily Data ' + str(year) + '\\Averages'):
            os.makedirs('data\\Daily Data ' + str(year) + '\\Averages')

        if not os.path.exists('data\\Daily Data ' + str(year) + '\\Averages\\Months'):
            os.makedirs('data\\Daily Data ' + str(year) + '\\Averages\\Months')

        ofile = open('data\\Daily Data ' + str(year) + '\\Averages\\Months\\HourlySensorData' + month + '.csv', "w",
                     newline='')
        writer = csv.writer(ofile, delimiter=',', quotechar='"', quoting=csv.QUOTE_ALL)

        writer.writerow(["Name", "Latitude", "Longitude", "Date", "Hour", "Variable", "Value"])

        for dataObj in outputData:

            csv_date = dataObj.date

            for i in range(0, 24):
                csv_hour = dataObj.hourlyAverages[i].hour

                csvRow = []

                csvRow.append(dataObj.name)
                csvRow.append(dataObj.latitude)
                csvRow.append(dataObj.longitude)

                csvRow.append(csv_date)
                csvRow.append(csv_hour)

                csvRow.append(dataObj.parameter)
                csvRow.append(dataObj.hourlyAverages[i].averageReading.GetAverage)  # NO2

                # print(csvRow)
                writer.writerow(csvRow)

        ofile.close()

        del outputData[:]

    print("Hourly Sensor Data Finished at " + time.ctime())
